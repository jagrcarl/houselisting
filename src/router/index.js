import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Agent from '../components/Agent.vue'
import House from '../components/House.vue'
Vue.use(VueRouter)
  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/houses',
    name: 'Houses',
    component: () => import('../views/Houses.vue')
  },
  {
    path: '/agents/',
    name: 'Agents',
    component: () => import('../views/Agents.vue')
  },
  {     
    path: '/agents/:id',
    name: 'Agent',
    component: Agent
  },
  {
    path: '/house/:id/:index',
    name: 'house',
    component: House
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
export default router